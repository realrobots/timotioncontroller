## RealRobots LiftController Mk 2
##### A wifi enabled lift controller board, designed to communicate with RS232 enabled smart actuators

### Firmware Update
Firmware can be updated OTA using a wifi connection.

Hold ESTOP and RESET to start in UPDATE mode.
If the device connects and downloads update the indicator light will flicker quickly as update is installed.

If WiFi connection fails the device will start in Access Point mode (5 flashes on indicator LED).
Connect to "RealRobotsLift" wifi (no password). Use a browser to connect to 192.168.4.1
A list of available wifi connections should appear.
After entering the desired ssid and password the device will restart and attempt to connect to the WiFi and update

To forget the currently configured WiFi credentials, hold the UP button as well as ESTOP and RESET.


![image-20210120130520480](./board_pcb_trans.jpg)
