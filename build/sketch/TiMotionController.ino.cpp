#include <Arduino.h>
#line 1 "e:\\Documents\\timotioncontroller\\TiMotionController\\TiMotionController.ino"

// INPUT PINS
#define PIN_ALARM 39
#define PIN_DOOR_1_SENSE 35
#define PIN_DOOR_2_SENSE 34
#define PIN_LIFT_CLEAR 32
#define PIN_LIFT_MIN 25
#define PIN_LIFT_MAX 33
#define PIN_ESTOP 26
#define PIN_DOWN 27
#define PIN_UP 19

// OUTPUT PINS
#define PIN_DOOR_1 5
#define PIN_DOOR_2 18
#define PIN_BUZZER 4
#define PIN_LED_INDICATOR 2

int inputPins[] = {PIN_ALARM, PIN_DOOR_1_SENSE, PIN_DOOR_2_SENSE,
                   PIN_LIFT_CLEAR, PIN_LIFT_MIN, PIN_LIFT_MAX, PIN_ESTOP,
                   PIN_DOWN, PIN_UP};

String pinLabels[] = {"ALARM", "DOOR1", "DOOR2", "LIFT_CLEAR", "LIFT_MIN",
                      "LIFT_MAX", "ESTOP", "DOWN", "UP"};

#define MODE_IDLE 0
#define MODE_MOVE_ABS 1
#define MODE_UPDATE_MODE 9

int tgtUp = 200;
int tgtDown = 20;

int moveTarget = 0;

int currentMode = MODE_IDLE;

bool btnUpState = 0;
bool btnDownState = 0;
bool btnESTOPState = 0;

int flashCount = 1;
int currentFlashCount = 0;
long lastFlash = 0;
int longFlashInterval = 3000;
int shortFlashInterval = 300;

#line 47 "e:\\Documents\\timotioncontroller\\TiMotionController\\TiMotionController.ino"
void setup();
#line 84 "e:\\Documents\\timotioncontroller\\TiMotionController\\TiMotionController.ino"
void loop();
#line 125 "e:\\Documents\\timotioncontroller\\TiMotionController\\TiMotionController.ino"
void SetCurrentMode(int mode);
#line 132 "e:\\Documents\\timotioncontroller\\TiMotionController\\TiMotionController.ino"
bool HasReachedTargetPos();
#line 147 "e:\\Documents\\timotioncontroller\\TiMotionController\\TiMotionController.ino"
void CheckButtons();
#line 178 "e:\\Documents\\timotioncontroller\\TiMotionController\\TiMotionController.ino"
int Difference(int v0, int v1);
#line 190 "e:\\Documents\\timotioncontroller\\TiMotionController\\TiMotionController.ino"
void SetIndicatorFlashCount(int count);
#line 197 "e:\\Documents\\timotioncontroller\\TiMotionController\\TiMotionController.ino"
void FlashIndicator();
#line 29 "e:\\Documents\\timotioncontroller\\TiMotionController\\OTAUpdate.ino"
void InitEEPROM();
#line 50 "e:\\Documents\\timotioncontroller\\TiMotionController\\OTAUpdate.ino"
void InitWifi();
#line 101 "e:\\Documents\\timotioncontroller\\TiMotionController\\OTAUpdate.ino"
void launchWeb();
#line 116 "e:\\Documents\\timotioncontroller\\TiMotionController\\OTAUpdate.ino"
void setupAP(void);
#line 163 "e:\\Documents\\timotioncontroller\\TiMotionController\\OTAUpdate.ino"
void createWebServer();
#line 240 "e:\\Documents\\timotioncontroller\\TiMotionController\\OTAUpdate.ino"
void ServerLoop();
#line 245 "e:\\Documents\\timotioncontroller\\TiMotionController\\OTAUpdate.ino"
void CheckForUpdate();
#line 319 "e:\\Documents\\timotioncontroller\\TiMotionController\\OTAUpdate.ino"
void InitOTA(const char *updateLink);
#line 372 "e:\\Documents\\timotioncontroller\\TiMotionController\\OTAUpdate.ino"
void updateFirmware(uint8_t *data, size_t len);
#line 1 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms.ino"
void InitComms();
#line 35 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
void InitComms232();
#line 42 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
void Process232();
#line 91 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
void InterpretIncoming();
#line 128 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
void TranslateStatusMessage();
#line 193 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
bool IsStatusMessage();
#line 198 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
void Relay();
#line 222 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
void CmdStop();
#line 230 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
void CmdExtend();
#line 238 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
void CmdRetract();
#line 246 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
void CmdGetStatus();
#line 257 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
void AwaitingAck();
#line 264 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
void AwaitingStatus();
#line 271 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
bool Send(const uint8_t *buffer, size_t size);
#line 285 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
void CmdMoveTo(int pos);
#line 318 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
bool IsAck();
#line 330 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
void ClearIncoming();
#line 20 "e:\\Documents\\timotioncontroller\\TiMotionController\\lift.ino"
void PrintStatus();
#line 25 "e:\\Documents\\timotioncontroller\\TiMotionController\\lift.ino"
void SetCurrentVoltage(int v);
#line 29 "e:\\Documents\\timotioncontroller\\TiMotionController\\lift.ino"
int GetCurrentVoltage();
#line 33 "e:\\Documents\\timotioncontroller\\TiMotionController\\lift.ino"
void SetCurrentCurrent(int a);
#line 37 "e:\\Documents\\timotioncontroller\\TiMotionController\\lift.ino"
int GetCurrentCurrent();
#line 41 "e:\\Documents\\timotioncontroller\\TiMotionController\\lift.ino"
void SetCurrentPosition(int pos);
#line 45 "e:\\Documents\\timotioncontroller\\TiMotionController\\lift.ino"
int GetCurrentPosition();
#line 49 "e:\\Documents\\timotioncontroller\\TiMotionController\\lift.ino"
void SetCurrentSpeed(int speed);
#line 53 "e:\\Documents\\timotioncontroller\\TiMotionController\\lift.ino"
int GetCurrentSpeed();
#line 57 "e:\\Documents\\timotioncontroller\\TiMotionController\\lift.ino"
void SetCurrentState(int state);
#line 61 "e:\\Documents\\timotioncontroller\\TiMotionController\\lift.ino"
int GetCurrentState();
#line 47 "e:\\Documents\\timotioncontroller\\TiMotionController\\TiMotionController.ino"
void setup()
{
  
  InitComms();
  InitComms232();
  InitEEPROM();
  for (int i = 0; i < 9; i++)
  {
    pinMode(inputPins[i], INPUT);
  }

  pinMode(PIN_BUZZER, OUTPUT);
  pinMode(PIN_DOOR_1, OUTPUT);
  pinMode(PIN_DOOR_2, OUTPUT);
  digitalWrite(PIN_BUZZER, 0);
  digitalWrite(PIN_DOOR_1, 0);
  digitalWrite(PIN_DOOR_2, 0);

  pinMode(PIN_LED_INDICATOR, OUTPUT);

  SetIndicatorFlashCount(1);

  // InitWifi();
  if (digitalRead(PIN_ESTOP))
  {
    Serial.println("ESTOP held during boot...");
    Serial.println("OverTheAir Update Initiated");
    // for (int i = 10; i >= 0; i--){
    //   Serial.println(i);
    //   delay(1000);
    // }
    SetCurrentMode(MODE_UPDATE_MODE);
    SetIndicatorFlashCount(5);
    InitWifi();
  }
}

void loop()
{
   FlashIndicator();
  if (currentMode == MODE_UPDATE_MODE)
  {
    ServerLoop();
    return;
  }

  switch (currentMode)
  {
  case MODE_IDLE:
    CheckButtons();
    break;
  case MODE_MOVE_ABS:
    if (HasReachedTargetPos())
    {
      SetCurrentMode(MODE_IDLE);
    }
    Process232();
    break;
  }

  if (digitalRead(PIN_ESTOP) && !btnESTOPState)
  {
    SetCurrentMode(MODE_IDLE);
    CmdStop();
    Serial.println("Pressed ESTOP");
    btnESTOPState = 1;
  }
  else if (!digitalRead(PIN_ESTOP) && btnESTOPState)
  {
    btnESTOPState = 0;
  }


  digitalWrite(PIN_BUZZER, digitalRead(PIN_ALARM));

  delay(1);
}

void SetCurrentMode(int mode)
{
  Serial.print("Mode changed: ");
  Serial.println(mode);
  currentMode = mode;
}

bool HasReachedTargetPos()
{
  Serial.print("DIFF:");
  Serial.println(Difference(GetCurrentPosition(), moveTarget));
  Serial.println(GetCurrentState());
  if (Difference(GetCurrentPosition(), moveTarget) < 2)
  {
    if (GetCurrentState() == 0)
    {
      return true;
    }
  }
  return false;
}

void CheckButtons()
{
  if (digitalRead(PIN_UP) && !btnUpState)
  {
    SetCurrentMode(MODE_MOVE_ABS);
    CmdMoveTo(tgtUp);
    moveTarget = tgtUp;
    Serial.println("Pressed Move up");
    btnUpState = 1;
    delay(500);
  }
  else if (!digitalRead(PIN_UP) && btnUpState)
  {
    btnUpState = 0;
  }

  if (digitalRead(PIN_DOWN) && !btnDownState)
  {
    SetCurrentMode(MODE_MOVE_ABS);
    CmdMoveTo(tgtDown);
    moveTarget = tgtDown;
    Serial.println("Pressed Move Down");
    btnDownState = 1;
    delay(500);
  }
  else if (!digitalRead(PIN_DOWN) && btnDownState)
  {
    btnDownState = 0;
  }
}

int Difference(int v0, int v1)
{
  if (v0 > v1)
  {
    return v0 - v1;
  }
  else
  {
    return v1 - v0;
  }
}

void SetIndicatorFlashCount(int count)
{
  flashCount = count;
  currentFlashCount = 0;
  digitalWrite(PIN_LED_INDICATOR, LOW);
}

void FlashIndicator()
{
  if (currentFlashCount < flashCount)
  {
    if (millis() - lastFlash > shortFlashInterval)
    {
      digitalWrite(PIN_LED_INDICATOR, !digitalRead(PIN_LED_INDICATOR));
      lastFlash = millis();

      if (!digitalRead(PIN_LED_INDICATOR))
      {
        currentFlashCount++;
      }
    }
  }
  else
  {
    if (millis() - lastFlash > longFlashInterval)
    {
      digitalWrite(PIN_LED_INDICATOR, !digitalRead(PIN_LED_INDICATOR));
      lastFlash = millis();
      currentFlashCount = 0;
    }
  }
}

#line 1 "e:\\Documents\\timotioncontroller\\TiMotionController\\OTAUpdate.ino"
#include <WiFi.h>
#include <HTTPClient.h>
#include <WebServer.h>
#include <EEPROM.h>
#include <Arduino_JSON.h>
#include <Update.h>

HTTPClient http;
// const char *ssid = "Police Surveillance Van";
// const char *password = "ourpasswor";
#define HOST "https://www.realrobots.net/files/ota/timot_version.json"
int totalLength;       // total size of firmware
int currentLength = 0; // current size of written firmware

#define CURRENT_VERSION 0

// AP Variables
int i = 0;
int statusCode;
// const char* ssid = "Default SSID";
// const char* passphrase = "Default passord";
const char *APName = "RealRobotsLift";
String st;
String content;
String esid;
String epass = "";
WebServer server(80);

void InitEEPROM()
{
    EEPROM.begin(512); // Initialasing EEPROM
    

    for (int i = 0; i < 32; ++i)
    {
        // EEPROM.write(i, 0);
        esid += char(EEPROM.read(i));
    }
    // EEPROM.commit();
    // Serial.println();
    // Serial.print("SSID: ");
    // Serial.println(esid);
    // Serial.println("Reading EEPROM pass");

    for (int i = 32; i < 96; ++i)
    {
        epass += char(EEPROM.read(i));
    }
}
void InitWifi()
{
    if (digitalRead(PIN_UP))
    {
        Serial.println("Clearing EEPROM");
        for (int i = 0; i < 32; ++i)
        {
            EEPROM.write(i, 0);
        }
        EEPROM.commit();
    }
    
    WiFi.mode(WIFI_MODE_STA);
    WiFi.begin(esid.c_str(), epass.c_str());
    
    Serial.print("Attempting to connect to wifi ssid: ");
    Serial.println(esid);
    int timeout = 0;
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
        digitalWrite(PIN_LED_INDICATOR, !digitalRead(PIN_LED_INDICATOR));
        timeout++;
        if (timeout == 20)
        {
            Serial.println();
            Serial.println("Connection to wifi failed.");
            break;
        }
    }

    if (WiFi.status() == WL_CONNECTED)
    {
        Serial.println("");
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());

        CheckForUpdate();
    }
    else
    {
        Serial.print("Initializing Access Point ");
        Serial.print(APName);
        Serial.println(" for wifi credential entry...");
        launchWeb();
        setupAP(); // Setup HotSpot
    }
}

void launchWeb()
{
    Serial.println("");
    if (WiFi.status() == WL_CONNECTED)
        Serial.println("WiFi connected");
    Serial.print("Local IP: ");
    Serial.println(WiFi.localIP());
    Serial.print("SoftAP IP: ");
    Serial.println(WiFi.softAPIP());
    createWebServer();
    // Start the server
    server.begin();
    Serial.println("Server started");
}

void setupAP(void)
{
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    delay(100);
    int n = WiFi.scanNetworks();
    Serial.println("scan done");
    if (n == 0)
        Serial.println("no networks found");
    else
    {
        Serial.print(n);
        Serial.println(" networks found");
        for (int i = 0; i < n; ++i)
        {
            // Print SSID and RSSI for each network found
            Serial.print(i + 1);
            Serial.print(": ");
            Serial.print(WiFi.SSID(i));
            Serial.print(" (");
            Serial.print(WiFi.RSSI(i));
            Serial.print(")");
            // Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
            delay(10);
        }
    }
    Serial.println("");
    st = "<ol>";
    for (int i = 0; i < n; ++i)
    {
        // Print SSID and RSSI for each network found
        st += "<li>";
        st += WiFi.SSID(i);
        st += " (";
        st += WiFi.RSSI(i);

        st += ")";
        // st += (WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*";
        st += "</li>";
    }
    st += "</ol>";
    delay(100);
    WiFi.softAP(APName, "");
    Serial.println("Initializing_softap_for_wifi credentials_modification");
    launchWeb();
}

void createWebServer()
{
    {
        server.on("/", []()
                  {

      IPAddress ip = WiFi.softAPIP();
      String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
      content = "<!DOCTYPE HTML>\r\n<html>Welcome to Wifi Credentials Update page";
      content += "<form action=\"/scan\" method=\"POST\"><input type=\"submit\" value=\"scan\"></form>";
      content += ipStr;
      content += "<p>";
      content += st;
      content += "</p><form method='get' action='setting'><label>SSID: </label><input name='ssid' length=32><input name='pass' length=64><input type='submit'></form>";
      content += "</html>";
      server.send(200, "text/html", content); });
        server.on("/scan", []()
                  {
      //setupAP();
      IPAddress ip = WiFi.softAPIP();
      String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);

      content = "<!DOCTYPE HTML>\r\n<html>go back";
      server.send(200, "text/html", content); });

        server.on("/setting", []()
                  {
                      String qsid = server.arg("ssid");
                      String qpass = server.arg("pass");
                      if (qsid.length() > 0 && qpass.length() > 0)
                      {
                          Serial.println("clearing eeprom");
                          for (int i = 0; i < 96; ++i)
                          {
                              EEPROM.write(i, 0);
                          }
                          Serial.println(qsid);
                          Serial.println("");
                          Serial.println(qpass);
                          Serial.println("");

                          Serial.println("writing eeprom ssid:");
                          for (int i = 0; i < qsid.length(); ++i)
                          {
                              EEPROM.write(i, qsid[i]);
                              Serial.print("Wrote: ");
                              Serial.println(qsid[i]);
                          }
                          Serial.println("writing eeprom pass:");
                          for (int i = 0; i < qpass.length(); ++i)
                          {
                              EEPROM.write(32 + i, qpass[i]);
                              Serial.print("Wrote: ");
                              Serial.println(qpass[i]);
                          }
                          EEPROM.commit();

                          content = "{\"Success\":\"saved to eeprom... reset to boot into new wifi\"}";
                          statusCode = 200;
                          //ESP.restart();
                          esid = qsid;
                          epass = qpass;
                          WiFi.disconnect();
                          delay(1000);
                          InitWifi();
                      }
                      else
                      {
                          content = "{\"Error\":\"404 not found\"}";
                          statusCode = 404;
                          Serial.println("Sending 404");
                      }
                      server.sendHeader("Access-Control-Allow-Origin", "*");
                      server.send(statusCode, "application/json", content); });
    }
}

void ServerLoop()
{
    server.handleClient();
}

void CheckForUpdate()
{
    Serial.println("Connecting to update server...");
    http.begin("https://www.realrobots.net/files/ota/timot_version.json"); // HTTP

    // start connection and send HTTP header
    int httpCode = http.GET();

    if (httpCode > 0)
    {

        // file found at server
        if (httpCode == HTTP_CODE_OK)
        {
            bool fileValid = true;
            String payload = http.getString();

            JSONVar myObject = JSON.parse(payload);
            if (JSON.typeof(myObject) == "undefined")
            {
                Serial.println("Parsing input failed!");
                return;
            }

            // Serial.print("JSON.typeof(myObject) = ");
            // Serial.println(JSON.typeof(myObject)); // prints: object

            // myObject.hasOwnProperty(key) checks if the object contains an entry for key
            if (myObject.hasOwnProperty("latestVersion"))
            {
                // Serial.print("myObject[\"latestVersion\"] = ");

                // Serial.println((int)myObject["latestVersion"]);
            }
            else
            {
                fileValid = false;
            }

            if (myObject.hasOwnProperty("latestBinary"))
            {
                // Serial.print("myObject[\"latestBinary\"] = ");

                // Serial.println((const char *)myObject["latestBinary"]);
            }
            else
            {
                fileValid = false;
            }

            if (fileValid)
            {
                if ((int)myObject["latestVersion"] != CURRENT_VERSION)
                {
                    Serial.println("New version available");
                    Serial.println((int)myObject["latestVersion"]);
                    Serial.println((const char *)myObject["latestBinary"]);
                    InitOTA((const char *)myObject["latestBinary"]);
                }
                else
                {
                    Serial.println("Current version up to date.");
                }
            }
        }
    }
    else
    {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
}

void InitOTA(const char *updateLink)
{

    // Connect to external web server
    http.begin(updateLink);
    // Get file, just to check if each reachable
    int resp = http.GET();
    Serial.print("Response: ");
    Serial.println(resp);
    // If file is reachable, start downloading
    if (resp == 200)
    {
        // get length of document (is -1 when Server sends no Content-Length header)
        totalLength = http.getSize();
        // transfer to local variable
        int len = totalLength;
        // this is required to start firmware update process
        Update.begin(UPDATE_SIZE_UNKNOWN);
        Serial.printf("FW Size: %u\n", totalLength);
        // create buffer for read
        uint8_t buff[128] = {0};
        // get tcp stream
        WiFiClient *stream = http.getStreamPtr();
        // read all data from server
        Serial.println("Updating firmware...");
        while (http.connected() && (len > 0 || len == -1))
        {
            // get available data size
            size_t size = stream->available();
            if (size)
            {
                // read up to 128 byte
                int c = stream->readBytes(buff, ((size > sizeof(buff)) ? sizeof(buff) : size));
                // pass to function
                updateFirmware(buff, c);
                if (len > 0)
                {
                    len -= c;
                }
            }
            delay(1);
        }
    }
    else
    {
        Serial.println("Cannot download firmware file. Only HTTP response 200: OK is supported. Double check firmware location #defined in HOST.");
    }
    http.end();
}

// Function to update firmware incrementally
// Buffer is declared to be 128 so chunks of 128 bytes
// from firmware is written to device until server closes
void updateFirmware(uint8_t *data, size_t len)
{
    digitalWrite(PIN_LED_INDICATOR, !digitalRead(PIN_LED_INDICATOR));
    Update.write(data, len);
    currentLength += len;
    // Print dots while waiting for update to finish
    Serial.println(currentLength);
    // if current length of written firmware is not equal to total firmware size, repeat
    if (currentLength != totalLength)
        return;
    Update.end(true);
    Serial.printf("\nUpdate Success, Total Size: %u\nRebooting...\n", currentLength);
    // Restart ESP32 to see changes
    ESP.restart();
}

#line 1 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms.ino"
void InitComms()
{
    Serial.begin(115200);
}
#line 1 "e:\\Documents\\timotioncontroller\\TiMotionController\\comms232.ino"
#define AUTO_STATUS_REQUEST true

#define NONE 0

#define MSG_STATUS_REQUEST 1
#define MSG_MOVE_COMMAND 2

#define RESPONSE_ACK 1
#define RESPONSE_STATUS 2

int messageSent = NONE;

int stp = 0;

int awaitingReasponseTimeout = 2000;
bool awaitingResponse = false;
long awaitingResponseStart = 0;
int awaitingResponseType = NONE;
byte acknowledge[] = {0x00, 0x86, 0x00, 0x7A};
long lastReceive = 0;
bool received = false;
bool messageInBuffer = false;
byte messageBuffer[7]; // 7 byte message stored in buffer

int b;
byte incoming[32];
int incomingCount = 0;

long lastSend = 5000;

int statusRequestInterval = 2000;
long lastStateUpdate = 0;
bool sentStateRequest = false;

void InitComms232()
{
    Serial2.begin(9600, SERIAL_8N1, 16, 17, true);

    Serial.println("RS232 Comms Initialized");
}

void Process232()
{
    while (Serial.available())
    {
        Serial2.write(Serial.read());
    }

    while (Serial2.available())
    {
        incoming[incomingCount] = Serial2.read();
        // Serial2.write(incoming[incomingCount]);
        incomingCount++;
        received = true;
        lastReceive = millis();
    }

    if (received)
    {
        received = false;
        // Serial.println("rs232 msg received");
        // for (int i = 0; i < incomingCount; i++)
        // {
        //     Serial.print(incoming[i], HEX);
        //     Serial.print(" ");
        // }
        // Serial.println();

        InterpretIncoming();
    }

    if (AUTO_STATUS_REQUEST)
    {
        if (millis() - lastStateUpdate > statusRequestInterval && !awaitingResponse)
        {
            CmdGetStatus();
        }
    }

    if (awaitingResponse && millis() - awaitingResponseStart > awaitingReasponseTimeout){
        Serial.print("ERROR: Timed out waiting on response: ");
        if (awaitingResponseType == RESPONSE_ACK){
            Serial.println("RESPONSE_ACK");
        } else if (awaitingResponseType == RESPONSE_STATUS){
            Serial.println("RESPONSE_STATUS");
        }
        awaitingResponse = false;
    }
}

void InterpretIncoming()
{
    if (incomingCount > 3)
    {
        if (IsAck())
        {
            if (awaitingResponse && awaitingResponseType == RESPONSE_ACK)
            {
                awaitingResponse = false;
                if (messageInBuffer){
                    messageInBuffer = false;
                    Send(messageBuffer, sizeof(messageBuffer));
                    AwaitingAck();
                }
            }
            else
            {
                Serial.println("Error: Recieved unexpected ACK");
            }
        }
        else if (IsStatusMessage())
        {
            if (awaitingResponse && awaitingResponseType == RESPONSE_STATUS)
            {
                awaitingResponse = false;
                TranslateStatusMessage();
            }
            else
            {
                Serial.println("Error: Recieved unexpected STATUS");
            }
        }
    }

    ClearIncoming();
}

void TranslateStatusMessage()
{
    unsigned int resultVal = (uint16_t)incoming[3] << 8 | incoming[4];
    // Serial.print("Voltage: ");
    // Serial.print((double)resultVal/10);
    // Serial.println("v");
    SetCurrentVoltage(resultVal);

    resultVal = (uint16_t)incoming[5] << 8 | incoming[6];
    // Serial.print("Current: ");
    // Serial.print((double)resultVal/10);
    // Serial.println("a");
    SetCurrentCurrent(resultVal);

    resultVal = (uint16_t)incoming[7] << 8 | incoming[8];
    // Serial.print("Position: ");
    // Serial.print(resultVal);
    // Serial.println("mm");
    SetCurrentPosition(resultVal);

    resultVal = (uint16_t)incoming[9] << 8 | incoming[10];
    // Serial.print("Speed: ");
    // Serial.print((double)resultVal/10);
    // Serial.println("mm/s");

    resultVal = (uint16_t)incoming[11] << 8 | incoming[12];
    // Serial.print("Status: ");
    // Serial.println(resultVal);
    SetCurrentState(resultVal);
    if (resultVal == 0)
    {
        // Serial.println("STOPPED");
    }
    else if (resultVal == 1)
    {
        // Serial.println("EXTENDING");
    }
    else if (resultVal == 2)
    {
        // Serial.println("RETRACTING");
    }
    else if (resultVal == 8)
    {
        // Serial.println("CRASHED INTO ENDSTOP?");
    }
    else
    {
        // Serial.print("UNKNOWN STATUS VALUE: ");
        // Serial.println(resultVal);
    }

    int total = 0;
    for (int i = 0; i < incomingCount; i++)
    {
        total += incoming[i];
    }
    // Serial.print(incomingCount);
    // Serial.println(" bytes received");
    // Serial.print("TOTAL: ");
    // Serial.println(total);
    PrintStatus();
    lastStateUpdate = millis();
    sentStateRequest = false;
}

bool IsStatusMessage()
{
    return incoming[0] == 0x00 && incoming[1] == 0x03 && incoming[2] == 0x0A;
}

void Relay()
{
    if (Serial2.available())
    {
        b = Serial2.read();
        Serial.write(b);
    }

    while (Serial.available())
    {
        incoming[incomingCount] = Serial.read();
        Serial2.write(incoming[incomingCount]);
        incomingCount++;
        received = true;
        lastReceive = millis();
    }

    if (received && millis() - lastReceive > 20)
    {
        received = false;
        incomingCount = 0;
    }
}

void CmdStop()
{
    Serial.println("Sending STOP Command");
    byte message[] = {0x00, 0x06, 0x10, 0x00, 0x00, 0x00, 0xEA};
    // Serial2.write(message, sizeof(message));
    Send(message, sizeof(message));
}

void CmdExtend()
{
    Serial.println("Sending EXTEND Command");
    byte message[] = {0x00, 0x06, 0x10, 0x00, 0x10, 0x00, 0xDA};
    // Serial2.write(message, sizeof(message));
    Send(message, sizeof(message));
}

void CmdRetract()
{
    Serial.println("Sending RETRACT Command");
    byte message[] = {0x00, 0x06, 0x10, 0x00, 0x20, 0x00, 0xCA};
    // Serial2.write(message, sizeof(message));
    Send(message, sizeof(message));
}

void CmdGetStatus()
{
    Serial.println("Sending REQUEST STATUS Command");
    byte message[] = {0x00, 0x03, 0x13, 0x01, 0x05, 0xE4};
    // Serial2.write(message, sizeof(message));
    if (Send(message, sizeof(message)))
    {
        AwaitingStatus();
    }
}

void AwaitingAck()
{
    awaitingResponse = true;
    awaitingResponseStart = millis();
    awaitingResponseType = RESPONSE_ACK;
}

void AwaitingStatus()
{
    awaitingResponse = true;
    awaitingResponseStart = millis();
    awaitingResponseType = RESPONSE_STATUS;
}

bool Send(const uint8_t *buffer, size_t size)
{
    if (!awaitingResponse)
    {
        Serial2.write(buffer, size);
        return true;
    }
    else
    {
        Serial.println("ERROR: Tried to send message while awaiting Ack");
        return false;
    }
}

void CmdMoveTo(int pos)
{
    byte highByte = highByte(pos);
    byte lowByte = lowByte(pos);
    Serial.println("Sending Move To Command");
    byte message[] = {0x00, 0x06, 0x10, 0x00, 0x40, 0x00, 0xAA};

    // Serial2.write(message, sizeof(message));
    if (Send(message, sizeof(message)))
    {
        AwaitingAck();
    }

    // Send Absolute Move Position
    byte message2[] = {0x00, 0x06, 0x10, 0x00, 0x00, 0x00, 0x00};
    message2[4] = 0x30 + highByte;
    message2[5] = lowByte;

    int total = 0;
    for (int i = 0; i < 6; i++)
    {
        total += message2[i];
    }
    message2[6] = 256 - total;

    for (int i = 0; i < 7; i++)
    {
        messageBuffer[i] = message2[i];
    }
    messageInBuffer = true;
    // Message 2 stored in buffer, awaiting Ack
}

bool IsAck()
{
    for (int i = 0; i < sizeof(acknowledge); i++)
    {
        if (incoming[i] != acknowledge[i])
        {
            return false;
        }
    }
    return true;
}

void ClearIncoming()
{
    for (int i = 0; i < sizeof(incoming); i++)
    {
        incoming[i] = 0;
    }
    incomingCount = 0;
}
#line 1 "e:\\Documents\\timotioncontroller\\TiMotionController\\lift.ino"
// Current voltage in 0.1v increments
int currentVoltage = -1;

// Current current in 0.1a increments
int currentCurrent = -1;

// Current position in mm
int currentPosition = -1;

// Current speed in mm/s
int currentSpeed = -1;

// Current state
// 0 = IDLE
// 1 = EXTENDING
// 2 = RETRACTING
// 8 = STOPPED (HIT ENDSTOP)
int currentState = -1;

void PrintStatus(){
    Serial.print("POS:");
    Serial.println(currentPosition);
}

void SetCurrentVoltage(int v){
    currentVoltage = v;
}

int GetCurrentVoltage(){
    return currentVoltage;
}

void SetCurrentCurrent(int a){
    currentCurrent = a;
}

int GetCurrentCurrent(){
    return currentCurrent;
}

void SetCurrentPosition(int pos){
    currentPosition = pos;
}

int GetCurrentPosition(){
    return currentPosition;
}

void SetCurrentSpeed(int speed){
    currentSpeed = speed;
}

int GetCurrentSpeed(){
    return currentSpeed;
}

void SetCurrentState(int state){
    currentState = state;
}

int GetCurrentState(){
    return currentState;
}
#line 1 "e:\\Documents\\timotioncontroller\\TiMotionController\\log.ino"


