
#define AUTO_STATUS_REQUEST true

#define NONE 0

#define MSG_STATUS_REQUEST 1
#define MSG_MOVE_COMMAND 2

#define RESPONSE_ACK 1
#define RESPONSE_STATUS 2


int messageSent = NONE;

int stp = 0;

int awaitingResponseTimeout = 2000;
bool awaitingResponse = false;
long awaitingResponseStart = 0;
int awaitingResponseType = NONE;
byte acknowledge[] = {0x00, 0x86, 0x00, 0x7A};
long lastReceive = 0;
bool received = false;
bool messageInBuffer = false;

// Array of 8 messages
// When message added it's added at messageCount idx and messageCount in incremented
byte messageBuffer[8][14]; // 7 byte message stored in buffer
uint8_t messageLen[8];
uint8_t messageExpectedResponse[8];
uint8_t messageCount = 0;

int b;
byte incoming[32];
int incomingCount = 0;

long lastSend = 5000;

int statusRequestInterval = 200;
long lastStateUpdate = 0;
bool sentStateRequest = false;

void InitComms232()
{
    Serial2.begin(9600, SERIAL_8N1, 16, 17, true);

    Serial.println("RS232 Comms Initialized");
}

void Process232()
{
    while (Serial.available())
    {
        Serial2.write(Serial.read());
    }

    while (Serial2.available())
    {
        incoming[incomingCount] = Serial2.read();
        incomingCount++;
        received = true;
        lastReceive = millis();
    }

    if (received)
    {
        received = false;

        InterpretIncoming();
    }

    if (AUTO_STATUS_REQUEST)
    {
        if (millis() - lastStateUpdate > statusRequestInterval && !awaitingResponse)
        {
            //Serial.println("Request status");
            if (messageCount == 0){
                CmdGetStatus();
            }
            //
        }
    }

    if (awaitingResponse && millis() - awaitingResponseStart > awaitingResponseTimeout){
        Serial.print("ERROR: Timed out waiting on response: ");
        if (awaitingResponseType == RESPONSE_ACK){
            Serial.println("RESPONSE_ACK");
        } else if (awaitingResponseType == RESPONSE_STATUS){
            Serial.println("RESPONSE_STATUS");
        } else {
            Serial.println("UNKNOWN_RESPONSE");
        }
        awaitingResponse = false;
    }

    HandleOutgoingMessageBuffer();
}

void HandleOutgoingMessageBuffer(){
    if (!awaitingResponse && messageCount > 0){
        SendMessageFromBuffer();
        PopMessageFromBuffer();
    }
}

void InterpretIncoming()
{
    //Serial.println("message received");
    if (incomingCount > 3)
    {
        if (IsAck())
        {
            if (awaitingResponse && awaitingResponseType == RESPONSE_ACK)
            {
                awaitingResponse = false;
                // if (messageInBuffer){
                //     messageInBuffer = false;
                //     Send(messageBuffer, sizeof(messageBuffer));
                //     AwaitingAck();
                // }
            }
            else
            {
                Serial.println("Error: Recieved unexpected ACK");
            }
        }
        else if (IsStatusMessage())
        {
            if (awaitingResponse && awaitingResponseType == RESPONSE_STATUS)
            {
                awaitingResponse = false;
                TranslateStatusMessage();
            }
            else
            {
                Serial.println("Error: Recieved unexpected STATUS");
            }
        }
    }

    ClearIncoming();
}

void TranslateStatusMessage()
{
    unsigned int resultVal = (uint16_t)incoming[3] << 8 | incoming[4];
    // Serial.print("Voltage: ");
    // Serial.print((double)resultVal/10);
    // Serial.println("v");
    lift.SetCurrentVoltage(resultVal);

    resultVal = (uint16_t)incoming[5] << 8 | incoming[6];
    // Serial.print("Current: ");
    // Serial.print((double)resultVal/10);
    // Serial.println("a");
    lift.SetCurrentCurrent(resultVal);

    resultVal = (uint16_t)incoming[7] << 8 | incoming[8];
    // Serial.print("Position: ");
    // Serial.print(resultVal);
    // Serial.println("mm");
    lift.SetCurrentPosition(resultVal);

    resultVal = (uint16_t)incoming[9] << 8 | incoming[10];
    // Serial.print("Speed: ");
    // Serial.print((double)resultVal/10);
    // Serial.println("mm/s");
    lift.SetCurrentSpeed(resultVal);

    resultVal = (uint16_t)incoming[11] << 8 | incoming[12];
    // Serial.print("Status: ");
    // Serial.println(resultVal);
    lift.SetCurrentState(resultVal);
    if (resultVal == 0)
    {
        // Serial.println("STOPPED");
    }
    else if (resultVal == 1)
    {
        // Serial.println("EXTENDING");
    }
    else if (resultVal == 2)
    {
        // Serial.println("RETRACTING");
    }
    else if (resultVal == 8)
    {
        // Serial.println("CRASHED INTO ENDSTOP?");
    }
    else
    {
        // Serial.print("UNKNOWN STATUS VALUE: ");
        // Serial.println(resultVal);
    }

    int total = 0;
    for (int i = 0; i < incomingCount; i++)
    {
        total += incoming[i];
    }
    // Serial.print(incomingCount);
    // Serial.println(" bytes received");
    // Serial.print("TOTAL: ");
    // Serial.println(total);
    //Serial.println(lift.PrintStatus());
    lastStateUpdate = millis();
    sentStateRequest = false;
}

bool IsStatusMessage()
{
    return incoming[0] == 0x00 && incoming[1] == 0x03 && incoming[2] == 0x0A;
}

void Relay()
{
    if (Serial2.available())
    {
        b = Serial2.read();
        Serial.write(b);
    }

    while (Serial.available())
    {
        incoming[incomingCount] = Serial.read();
        Serial2.write(incoming[incomingCount]);
        incomingCount++;
        received = true;
        lastReceive = millis();
    }

    if (received && millis() - lastReceive > 20)
    {
        received = false;
        incomingCount = 0;
    }
}

void CmdStop()
{
    Serial.println("Sending STOP Command");
    byte message[] = {0x00, 0x06, 0x10, 0x00, 0x00, 0x00, 0xEA};
    Serial2.write(message, sizeof(message));
    // doesn't use Send() command which fails if awaiting response
}

void CmdExtend()
{
    Serial.println("Sending EXTEND Command");
    byte message[] = {0x00, 0x06, 0x10, 0x00, 0x10, 0x00, 0xDA};
    // Serial2.write(message, sizeof(message));
    Send(message, sizeof(message));
}

void CmdRetract()
{
    Serial.println("Sending RETRACT Command");
    byte message[] = {0x00, 0x06, 0x10, 0x00, 0x20, 0x00, 0xCA};
    // Serial2.write(message, sizeof(message));
    Send(message, sizeof(message));
}

void CmdGetStatus()
{
    //Serial.println("Sending REQUEST STATUS Command");
    byte message[] = {0x00, 0x03, 0x13, 0x01, 0x05, 0xE4};
    // Serial2.write(message, sizeof(message));

    

    AddMessageToBuffer(message, sizeof(message), RESPONSE_STATUS);
    // if (Send(message, sizeof(message)))
    // {
    //     AwaitingStatus();
    // }
}

void AwaitingAck()
{
    awaitingResponse = true;
    awaitingResponseStart = millis();
    awaitingResponseType = RESPONSE_ACK;
}

void AwaitingStatus()
{
    awaitingResponse = true;
    awaitingResponseStart = millis();
    awaitingResponseType = RESPONSE_STATUS;
}

void AddMessageToBuffer(const uint8_t *buffer, size_t size, uint8_t response){
   
    for (int i = 0; i < size; i++){
        messageBuffer[messageCount][i] = buffer[i];
    }
    messageLen[messageCount] = size;    
    messageExpectedResponse[messageCount] = response;
    messageCount++;
}

void SendMessageFromBuffer(){
    byte message[messageLen[messageCount-1]];
    for (int i = 0; i < messageLen[messageCount-1]; i++){
        message[i] = messageBuffer[messageCount-1][i];
    }
    if (messageExpectedResponse[messageCount-1] == RESPONSE_ACK){
        AwaitingAck();
    } else if (messageExpectedResponse[messageCount-1] == RESPONSE_STATUS){
        AwaitingStatus();
    }
    //Serial.println("Sent message");
    Serial2.write(message, sizeof(message));
}

void PopMessageFromBuffer(){
    for (int i = 1; i < 8; i++){
        for (int l = 0; l < 14; l++){
            messageBuffer[i-1][l] = messageBuffer[i][l];
        }
        messageLen[i-1] = messageLen[i];
        messageExpectedResponse[i-1] = messageLen[i];
    }
    messageCount--;
}

bool Send(const uint8_t *buffer, size_t size)
{
    if (!awaitingResponse)
    {
        Serial2.write(buffer, size);
        return true;
    }
    else
    {
        Serial.println("ERROR: Tried to send message while awaiting Ack");
        return false;
    }
}

void CmdMoveTo(int pos)
{

    byte highByte = highByte(pos);
    byte lowByte = lowByte(pos);
    Serial.println("Sending Move To Command");
    byte message[] = {0x00, 0x06, 0x10, 0x00, 0x40, 0x00, 0xAA};

    // Serial2.write(message, sizeof(message));
    // if (Send(message, sizeof(message)))
    // {
    //     AwaitingAck();
    // } else {
    //     Serial.println("ERROR: Can't send move command, awaiting resposnse");
    //     return;
    // }

    // Send Absolute Move Position
    byte message2[] = {0x00, 0x06, 0x10, 0x00, 0x00, 0x00, 0x00};
    message2[4] = 0x30 + highByte;
    message2[5] = lowByte;

    int total = 0;
    for (int i = 0; i < 6; i++)
    {
        total += message2[i];
    }
    message2[6] = 256 - total;

    AddMessageToBuffer(message, sizeof(message), RESPONSE_ACK);
    AddMessageToBuffer(message2, sizeof(message2), RESPONSE_ACK);

    // for (int i = 0; i < 7; i++)
    // {
    //     messageBuffer[i] = message2[i];
    // }
    // messageInBuffer = true;
    // Message 2 stored in buffer, awaiting Ack
}

bool IsAck()
{
    for (int i = 0; i < sizeof(acknowledge); i++)
    {
        if (incoming[i] != acknowledge[i])
        {
            return false;
        }
    }
    return true;
}

void ClearIncoming()
{
    for (int i = 0; i < sizeof(incoming); i++)
    {
        incoming[i] = 0;
    }
    incomingCount = 0;
}