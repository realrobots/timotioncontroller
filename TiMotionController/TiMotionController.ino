#include "lift.h"
// INPUT PINS
#define PIN_ALARM 39
#define PIN_DOOR_1_SENSE 35
#define PIN_DOOR_2_SENSE 34
#define PIN_LIFT_CLEAR 32
#define PIN_LIFT_MIN 25
#define PIN_LIFT_MAX 33
#define PIN_ESTOP 26
#define PIN_DOWN 27
#define PIN_UP 19

// OUTPUT PINS
#define PIN_DOOR_1 5
#define PIN_DOOR_2 18
#define PIN_BUZZER 4
#define PIN_LED_INDICATOR 2

int inputPins[] = {PIN_ALARM, PIN_DOOR_1_SENSE, PIN_DOOR_2_SENSE,
                   PIN_LIFT_CLEAR, PIN_LIFT_MIN, PIN_LIFT_MAX, PIN_ESTOP,
                   PIN_DOWN, PIN_UP};

String pinLabels[] = {"ALARM", "DOOR1", "DOOR2", "LIFT_CLEAR", "LIFT_MIN",
                      "LIFT_MAX", "ESTOP", "DOWN", "UP"};

#define MODE_IDLE 0
#define MODE_MOVE_ABS 1
#define MODE_UPDATE_MODE 9

Lift lift;

int tgtUp = 200;
int tgtDown = 20;

int moveTarget = 0;

int currentMode = MODE_IDLE;

bool btnUpState = 0;
bool btnDownState = 0;
bool btnESTOPState = 0;

int flashCount = 1;
int currentFlashCount = 0;
long lastFlash = 0;
int longFlashInterval = 3000;
int shortFlashInterval = 300;

void setup()
{

  InitComms();
  InitComms232();
  InitEEPROM();
  for (int i = 0; i < 9; i++)
  {
    pinMode(inputPins[i], INPUT);
  }

  pinMode(PIN_BUZZER, OUTPUT);
  pinMode(PIN_DOOR_1, OUTPUT);
  pinMode(PIN_DOOR_2, OUTPUT);
  digitalWrite(PIN_BUZZER, 0);
  digitalWrite(PIN_DOOR_1, 0);
  digitalWrite(PIN_DOOR_2, 0);

  pinMode(PIN_LED_INDICATOR, OUTPUT);

  SetIndicatorFlashCount(1);

  // InitWifi();
  if (digitalRead(PIN_ESTOP))
  {
    Serial.println("ESTOP held during boot...");
    Serial.println("OverTheAir Update Initiated");
    // for (int i = 10; i >= 0; i--){
    //   Serial.println(i);
    //   delay(1000);
    // }
    SetCurrentMode(MODE_UPDATE_MODE);
    SetIndicatorFlashCount(5);
    InitWifi();
  }
}

void loop()
{
  FlashIndicator();
  if (currentMode == MODE_UPDATE_MODE)
  {
    ServerLoop();
    return;
  }
  CheckESTOP();

  Process232();
  CheckButtons();
  digitalWrite(PIN_BUZZER, digitalRead(PIN_ALARM));
  delay(1);
  return;

  switch (currentMode)
  {
  case MODE_IDLE:
    CheckButtons();
    break;
  case MODE_MOVE_ABS:
    if (HasReachedTargetPos())
    {
      SetCurrentMode(MODE_IDLE);
    }
    Process232();
    break;
  }

  

  

  delay(1);
}

void CheckESTOP(){
if (digitalRead(PIN_ESTOP) && !btnESTOPState)
  {
    SetCurrentMode(MODE_IDLE);
    CmdStop();
    Serial.println("Pressed ESTOP");
    btnESTOPState = 1;
  }
  else if (!digitalRead(PIN_ESTOP) && btnESTOPState)
  {
    btnESTOPState = 0;
  }
}

void SetCurrentMode(int mode)
{
  Serial.print("Mode changed: ");
  Serial.println(mode);
  currentMode = mode;
}

bool HasReachedTargetPos()
{
  // Serial.print("DIFF:");
  // Serial.println(Difference(GetCurrentPosition(), moveTarget));
  // Serial.println(GetCurrentState());
  if (Difference(lift.GetCurrentPosition(), moveTarget) < 2)
  {
    if (lift.GetCurrentState() == 0)
    {
      return true;
    }
  }
  return false;
}

void CheckButtons()
{
  if (digitalRead(PIN_UP) && !btnUpState)
  {
    SetCurrentMode(MODE_MOVE_ABS);
    CmdMoveTo(tgtUp);
    moveTarget = tgtUp;
    Serial.println("Pressed Move up");
    btnUpState = 1;
    delay(500);
  }
  else if (!digitalRead(PIN_UP) && btnUpState)
  {
    btnUpState = 0;
  }

  if (digitalRead(PIN_DOWN) && !btnDownState)
  {
    SetCurrentMode(MODE_MOVE_ABS);
    CmdMoveTo(tgtDown);
    moveTarget = tgtDown;
    Serial.println("Pressed Move Down");
    btnDownState = 1;
    delay(500);
  }
  else if (!digitalRead(PIN_DOWN) && btnDownState)
  {
    btnDownState = 0;
  }
}

int Difference(int v0, int v1)
{
  if (v0 > v1)
  {
    return v0 - v1;
  }
  else
  {
    return v1 - v0;
  }
}

void SetIndicatorFlashCount(int count)
{
  flashCount = count;
  currentFlashCount = 0;
  digitalWrite(PIN_LED_INDICATOR, LOW);
}

void FlashIndicator()
{
  if (currentFlashCount < flashCount)
  {
    if (millis() - lastFlash > shortFlashInterval)
    {
      digitalWrite(PIN_LED_INDICATOR, !digitalRead(PIN_LED_INDICATOR));
      lastFlash = millis();

      if (!digitalRead(PIN_LED_INDICATOR))
      {
        currentFlashCount++;
      }
    }
  }
  else
  {
    if (millis() - lastFlash > longFlashInterval)
    {
      digitalWrite(PIN_LED_INDICATOR, !digitalRead(PIN_LED_INDICATOR));
      lastFlash = millis();
      currentFlashCount = 0;
    }
  }
}
