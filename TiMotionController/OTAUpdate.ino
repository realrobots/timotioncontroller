#include <WiFi.h>
#include <HTTPClient.h>
#include <WebServer.h>
#include <EEPROM.h>
#include <Arduino_JSON.h>
#include <Update.h>

HTTPClient http;
// const char *ssid = "Police Surveillance Van";
// const char *password = "ourpasswor";
#define HOST "https://www.realrobots.net/files/ota/timot_version.json"
int totalLength;       // total size of firmware
int currentLength = 0; // current size of written firmware

#define CURRENT_VERSION 0

// AP Variables
int i = 0;
int statusCode;
// const char* ssid = "Default SSID";
// const char* passphrase = "Default passord";
const char *APName = "RealRobotsLift";
String st;
String content;
String esid;
String epass = "";
WebServer server(80);

void InitEEPROM()
{
    EEPROM.begin(512); // Initialasing EEPROM
    

    for (int i = 0; i < 32; ++i)
    {
        // EEPROM.write(i, 0);
        esid += char(EEPROM.read(i));
    }
    // EEPROM.commit();
    // Serial.println();
    // Serial.print("SSID: ");
    // Serial.println(esid);
    // Serial.println("Reading EEPROM pass");

    for (int i = 32; i < 96; ++i)
    {
        epass += char(EEPROM.read(i));
    }
}
void InitWifi()
{
    if (digitalRead(PIN_UP))
    {
        Serial.println("Clearing EEPROM");
        for (int i = 0; i < 32; ++i)
        {
            EEPROM.write(i, 0);
        }
        EEPROM.commit();
    }
    
    WiFi.mode(WIFI_MODE_STA);
    WiFi.begin(esid.c_str(), epass.c_str());
    
    Serial.print("Attempting to connect to wifi ssid: ");
    Serial.println(esid);
    int timeout = 0;
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
        digitalWrite(PIN_LED_INDICATOR, !digitalRead(PIN_LED_INDICATOR));
        timeout++;
        if (timeout == 20)
        {
            Serial.println();
            Serial.println("Connection to wifi failed.");
            break;
        }
    }

    if (WiFi.status() == WL_CONNECTED)
    {
        Serial.println("");
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());

        CheckForUpdate();
    }
    else
    {
        Serial.print("Initializing Access Point ");
        Serial.print(APName);
        Serial.println(" for wifi credential entry...");
        launchWeb();
        setupAP(); // Setup HotSpot
    }
}

void launchWeb()
{
    Serial.println("");
    if (WiFi.status() == WL_CONNECTED)
        Serial.println("WiFi connected");
    Serial.print("Local IP: ");
    Serial.println(WiFi.localIP());
    Serial.print("SoftAP IP: ");
    Serial.println(WiFi.softAPIP());
    createWebServer();
    // Start the server
    server.begin();
    Serial.println("Server started");
}

void setupAP(void)
{
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    delay(100);
    int n = WiFi.scanNetworks();
    Serial.println("scan done");
    if (n == 0)
        Serial.println("no networks found");
    else
    {
        Serial.print(n);
        Serial.println(" networks found");
        for (int i = 0; i < n; ++i)
        {
            // Print SSID and RSSI for each network found
            Serial.print(i + 1);
            Serial.print(": ");
            Serial.print(WiFi.SSID(i));
            Serial.print(" (");
            Serial.print(WiFi.RSSI(i));
            Serial.print(")");
            // Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
            delay(10);
        }
    }
    Serial.println("");
    st = "<ol>";
    for (int i = 0; i < n; ++i)
    {
        // Print SSID and RSSI for each network found
        st += "<li>";
        st += WiFi.SSID(i);
        st += " (";
        st += WiFi.RSSI(i);

        st += ")";
        // st += (WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*";
        st += "</li>";
    }
    st += "</ol>";
    delay(100);
    WiFi.softAP(APName, "");
    Serial.println("Initializing_softap_for_wifi credentials_modification");
    launchWeb();
}

void createWebServer()
{
    {
        server.on("/", []()
                  {

      IPAddress ip = WiFi.softAPIP();
      String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
      content = "<!DOCTYPE HTML>\r\n<html>Welcome to Wifi Credentials Update page";
      content += "<form action=\"/scan\" method=\"POST\"><input type=\"submit\" value=\"scan\"></form>";
      content += ipStr;
      content += "<p>";
      content += st;
      content += "</p><form method='get' action='setting'><label>SSID: </label><input name='ssid' length=32><input name='pass' length=64><input type='submit'></form>";
      content += "</html>";
      server.send(200, "text/html", content); });
        server.on("/scan", []()
                  {
      //setupAP();
      IPAddress ip = WiFi.softAPIP();
      String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);

      content = "<!DOCTYPE HTML>\r\n<html>go back";
      server.send(200, "text/html", content); });

        server.on("/setting", []()
                  {
                      String qsid = server.arg("ssid");
                      String qpass = server.arg("pass");
                      if (qsid.length() > 0 && qpass.length() > 0)
                      {
                          Serial.println("clearing eeprom");
                          for (int i = 0; i < 96; ++i)
                          {
                              EEPROM.write(i, 0);
                          }
                          Serial.println(qsid);
                          Serial.println("");
                          Serial.println(qpass);
                          Serial.println("");

                          Serial.println("writing eeprom ssid:");
                          for (int i = 0; i < qsid.length(); ++i)
                          {
                              EEPROM.write(i, qsid[i]);
                              Serial.print("Wrote: ");
                              Serial.println(qsid[i]);
                          }
                          Serial.println("writing eeprom pass:");
                          for (int i = 0; i < qpass.length(); ++i)
                          {
                              EEPROM.write(32 + i, qpass[i]);
                              Serial.print("Wrote: ");
                              Serial.println(qpass[i]);
                          }
                          EEPROM.commit();

                          content = "{\"Success\":\"saved to eeprom... reset to boot into new wifi\"}";
                          statusCode = 200;
                          //ESP.restart();
                          esid = qsid;
                          epass = qpass;
                          WiFi.disconnect();
                          delay(1000);
                          InitWifi();
                      }
                      else
                      {
                          content = "{\"Error\":\"404 not found\"}";
                          statusCode = 404;
                          Serial.println("Sending 404");
                      }
                      server.sendHeader("Access-Control-Allow-Origin", "*");
                      server.send(statusCode, "application/json", content); });
    }
}

void ServerLoop()
{
    server.handleClient();
}

void CheckForUpdate()
{
    Serial.println("Connecting to update server...");
    http.begin("https://www.realrobots.net/files/ota/timot_version.json"); // HTTP

    // start connection and send HTTP header
    int httpCode = http.GET();

    if (httpCode > 0)
    {

        // file found at server
        if (httpCode == HTTP_CODE_OK)
        {
            bool fileValid = true;
            String payload = http.getString();

            JSONVar myObject = JSON.parse(payload);
            if (JSON.typeof(myObject) == "undefined")
            {
                Serial.println("Parsing input failed!");
                return;
            }

            // Serial.print("JSON.typeof(myObject) = ");
            // Serial.println(JSON.typeof(myObject)); // prints: object

            // myObject.hasOwnProperty(key) checks if the object contains an entry for key
            if (myObject.hasOwnProperty("latestVersion"))
            {
                // Serial.print("myObject[\"latestVersion\"] = ");

                // Serial.println((int)myObject["latestVersion"]);
            }
            else
            {
                fileValid = false;
            }

            if (myObject.hasOwnProperty("latestBinary"))
            {
                // Serial.print("myObject[\"latestBinary\"] = ");

                // Serial.println((const char *)myObject["latestBinary"]);
            }
            else
            {
                fileValid = false;
            }

            if (fileValid)
            {
                if ((int)myObject["latestVersion"] != CURRENT_VERSION)
                {
                    Serial.println("New version available");
                    Serial.println((int)myObject["latestVersion"]);
                    Serial.println((const char *)myObject["latestBinary"]);
                    InitOTA((const char *)myObject["latestBinary"]);
                }
                else
                {
                    Serial.println("Current version up to date.");
                }
            }
        }
    }
    else
    {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
}

void InitOTA(const char *updateLink)
{

    // Connect to external web server
    http.begin(updateLink);
    // Get file, just to check if each reachable
    int resp = http.GET();
    Serial.print("Response: ");
    Serial.println(resp);
    // If file is reachable, start downloading
    if (resp == 200)
    {
        // get length of document (is -1 when Server sends no Content-Length header)
        totalLength = http.getSize();
        // transfer to local variable
        int len = totalLength;
        // this is required to start firmware update process
        Update.begin(UPDATE_SIZE_UNKNOWN);
        Serial.printf("FW Size: %u\n", totalLength);
        // create buffer for read
        uint8_t buff[128] = {0};
        // get tcp stream
        WiFiClient *stream = http.getStreamPtr();
        // read all data from server
        Serial.println("Updating firmware...");
        while (http.connected() && (len > 0 || len == -1))
        {
            // get available data size
            size_t size = stream->available();
            if (size)
            {
                // read up to 128 byte
                int c = stream->readBytes(buff, ((size > sizeof(buff)) ? sizeof(buff) : size));
                // pass to function
                updateFirmware(buff, c);
                if (len > 0)
                {
                    len -= c;
                }
            }
            delay(1);
        }
    }
    else
    {
        Serial.println("Cannot download firmware file. Only HTTP response 200: OK is supported. Double check firmware location #defined in HOST.");
    }
    http.end();
}

// Function to update firmware incrementally
// Buffer is declared to be 128 so chunks of 128 bytes
// from firmware is written to device until server closes
void updateFirmware(uint8_t *data, size_t len)
{
    digitalWrite(PIN_LED_INDICATOR, !digitalRead(PIN_LED_INDICATOR));
    Update.write(data, len);
    currentLength += len;
    // Print dots while waiting for update to finish
    Serial.println(currentLength);
    // if current length of written firmware is not equal to total firmware size, repeat
    if (currentLength != totalLength)
        return;
    Update.end(true);
    Serial.printf("\nUpdate Success, Total Size: %u\nRebooting...\n", currentLength);
    // Restart ESP32 to see changes
    ESP.restart();
}
