class Lift
{
public:
    String PrintStatus()
    {
        String ret = "POS: ";
        ret += currentPosition;
        return ret;
    }

    void SetCurrentVoltage(int v)
    {
        currentVoltage = v;
    }

    int GetCurrentVoltage()
    {
        return currentVoltage;
    }

    void SetCurrentCurrent(int a)
    {
        currentCurrent = a;
    }

    int GetCurrentCurrent()
    {
        return currentCurrent;
    }

    void SetCurrentPosition(int pos)
    {
        currentPosition = pos;
    }

    int GetCurrentPosition()
    {
        return currentPosition;
    }

    void SetCurrentSpeed(int speed)
    {
        currentSpeed = speed;
    }

    int GetCurrentSpeed()
    {
        return currentSpeed;
    }

    void SetCurrentState(int state)
    {
        currentState = state;
    }

    int GetCurrentState()
    {
        return currentState;
    }

private:
    // Current voltage in 0.1v increments
    int currentVoltage = -1;

    // Current current in 0.1a increments
    int currentCurrent = -1;

    // Current position in mm
    int currentPosition = -1;

    // Current speed in mm/s
    int currentSpeed = -1;

    // Current state
    // 0 = IDLE
    // 1 = EXTENDING
    // 2 = RETRACTING
    // 8 = STOPPED (HIT ENDSTOP)
    int currentState = -1;
};
